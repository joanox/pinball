﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class throwBall : MonoBehaviour
{
    public GameObject Ball;
    Rigidbody rb;
    public float impulseForce;
    public bool ControlShoot = false;



    public void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
       
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ControlShoot = true;
          
        }
    }

    private void FixedUpdate()
    {
        if (ControlShoot == true) 
        {
            Ball.GetComponent<Rigidbody>().AddForce(new Vector3(0.0f, impulseForce, 0.0f), ForceMode.Impulse);
            ControlShoot = false; 
        }

    }
} 