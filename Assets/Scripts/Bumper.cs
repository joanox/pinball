﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bumper : MonoBehaviour {

    public GameObject Ball;
    public int force;
  

    public void Start()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name == "Ball")
        {
            Ball.GetComponent<Rigidbody>().AddForce(new Vector3(collision.gameObject.transform.position.x * -1, collision.gameObject.transform.position.y * -1, collision.gameObject.transform.position.z) * force);
        }
    }
}
